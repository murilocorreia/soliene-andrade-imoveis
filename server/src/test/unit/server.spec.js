const chai = require('chai');
const httpStatus = require('http-status');
const chaiHttp = require('chai-http');
const startServer = require('../../server/server');
const should = chai.should();

chai.use(chaiHttp);

const dados = [{
  nome: 'Reserva do Parque',
  localizacao: 'Serra',
}, {
  nome: 'San Diego',
  localizacao: 'Vitoria'
}]

class EmpreendimentoModel {
  constructor(nome, localizacao) {
    this.nome = nome;
    this.localizacao = localizacao;
  }

  static find(query) {
    return this;
  }
  static exec() {
    return new Promise((resolve, reject) => {
      resolve(dados);
    });
  }
}

const models = {
  Empreendimentos: EmpreendimentoModel,
}

let app;
before(() => {
  startServer(models).then((server) => {
    app = server;
    console.log('Server up');
  }).catch((err) => {
    console.log(err);
  });
});

after(() => {
  app.close();
});

describe('Server', () => {
  it('Buscou dados com sucesso', (done) => {
    try {
      chai.request(app).get('/empreendimentos')
        .then((res) => {
          res.should.have.status(httpStatus.OK);
          done();
        }).catch((err) => {
          console.log(err);
          done(err);
        });
    } catch (erro) {
      console.log(erro);
      done(erro);
    }
  });
});

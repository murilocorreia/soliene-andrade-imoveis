const Controller = require('../../empreendimentos/controllers/Controller');
const chai = require('chai');
const {
  AppError
} = require('../../error');
const should = chai.should();

class ServiceSucess {
  static buscaTodos() {
    return new Promise((resolve, reject) => {
      resolve();
    });
  }
}

class ServiceFail {
  static buscaTodos() {
    return new Promise((resolve, reject) => {
      reject(new AppError('teste', 500, 'teste', true));
    });
  }
}

class Response {
  constructor() {
    this.value = null;
    this.status = this.status.bind(this);
    this.json = this.json.bind(this);
    this.getStatus = this.getStatus.bind(this);
  }
  status(status) {
    this.value = status;
    return this;
  }
  getStatus() {
    return this.value;
  }

  json() {}

}

describe('Controller', () => {
  it('Sucesso ao buscar', async () => {
    try {
      let res = new Response();
      const controller = new Controller(ServiceSucess);
      await controller.buscaTodos({}, res);
      res.getStatus().should.be.equal(200);
    } catch (erro) {
      console.log(erro);
      throw erro;
    }
  });

  it('Falha ao buscar', async () => {
    try {
      let res = new Response();
      const controller = new Controller(ServiceFail);
      await controller.buscaTodos({}, res);
      res.getStatus().should.be.equal(500);
    } catch (erro) {
      console.log(erro);
      throw erro;
    }
  });
});

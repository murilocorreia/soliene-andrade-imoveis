const chai = require('chai');
const Service = require('../../empreendimentos/services/Service');
const {
  AppError
} = require('../../error');
const httpStatus = require('http-status');
const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
const should = chai.should();
//Classes com fluxo normal ao salvar
class ModeloMockSucesso {
  constructor(nome, localizacao) {
    this.nome = nome;
    this.localizacao = localizacao;
  }

  save() {
    return new Promise((resolve, reject) => {
      resolve("sucesso");
    });
  }

  static find(query, cb) {
    return new Promise((resolve, reject) => {
      resolve("sucesso");
    });
  }
}

//Classes para forcar erro
class ModeloMockFail {
  constructor(nome, localizacao) {
    this.nome = nome;
    this.localizacao = localizacao;
  }

  save() {
    return new Promise((resolve, reject) => {
      reject("erro");
    });
  }

  static find(query, cb) {
    cb("erro", "falha");
  }
}

class ModeloMockNoMethod {
  constructor(nome, localizacao) {
    this.nome = nome;
    this.localizacao = localizacao;
  }

}

//Mocks de dados
const empreendimentoMock = {
  nome: "teste",
  localizacao: "teste",
};

const empreendimentoMockFail = {
  nome: "teste",
};

//Testes de falha
describe('Service', () => {

  it('Sucesso ao gravar as informacoes', async () => {

    try {
      const service = new Service(ModeloMockSucesso);
      result = await service.gravar(empreendimentoMock);
      result.should.not.be.empty;
    } catch (erroInesperado) {
      return erroInesperado;
    }

  });

  it('Erro de validacao', async () => {

    try {
      const service = new Service(ModeloMockSucesso);
      return service.gravar(empreendimentoMockFail).should.be.rejectedWith(AppError);
    } catch (erroInesperado) {
      return erroInesperado;
    }

  });

  it('Erro ao gravar as informacoes', (done) => {

    try {
      const service = new Service(ModeloMockFail);
      service.gravar(empreendimentoMock).then(result => done('Deveria falhar'))
        .catch(error => {
          done();
        });

    } catch (erroInesperado) {
      done(erroInesperado);
    }
  });

  it('Erro generico ao gravar', (done) => {
    try {
      const service = new Service(ModeloMockNoMethod);
      service.gravar(empreendimentoMock).then(result => done('Deveria falhar'))
        .catch(error => {
          error.httpCode.should.be.equal(httpStatus.INTERNAL_SERVER_ERROR);
          done();
        });
    } catch (erroInesperado) {
      done(erroInesperado);
    }
  });

  it('Sucesso ao buscar dados', async () => {
    try {
      const service = new Service(ModeloMockSucesso);
      const dados = await service.buscaTodos();
      dados.should.not.be.empty;
    } catch (erroInesperado) {
      return erroInesperado;
    }
  });

  it('Falha ao buscar dados', (done) => {
    try {
      const service = new Service(ModeloMockFail);
      service.buscaTodos().then(result => done('Deveria falhar'))
        .catch(erro => {
          erro.httpCode.should.be.equal(httpStatus.INTERNAL_SERVER_ERROR);
          done();
        })
    } catch (erroInesperado) {
      done(erroInesperado);
    }
  });

  it('Erro generico ao buscar', (done) => {
    try {
      const service = new Service(ModeloMockNoMethod);
      service.buscaTodos().then(result => done('Deveria falhar'))
        .catch(error => {
          error.httpCode.should.be.equal(httpStatus.INTERNAL_SERVER_ERROR);
          done();
        });
    } catch (erroInesperado) {
      done(erroInesperado);
    }
  });

});

const AppError = require('./AppError');
const httpStatus = require('http-status');

module.exports = function handleError(error) {
  if (error instanceof AppError) {
    return error;
  } else {
    return new AppError(error.name, httpStatus.INTERNAL_SERVER_ERROR, error.message,
      false);
  }
}

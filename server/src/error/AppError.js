class AppError extends Error {
  constructor(name, httpCode, message, isOperational) {
    super(message);
    Error.captureStackTrace(this, this.constructor);
    this.name = name;
    this.httpCode = httpCode;
    this.message = message;
    this.isOperational = isOperational;
  }
}

module.exports = AppError;

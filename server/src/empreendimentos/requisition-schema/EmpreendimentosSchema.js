const Joi = require('joi');

const EmpreendimentosSchema = Joi.object().keys({

  // O nome e obrigatorio
  // Nome deve ser uma string valida
  nome: Joi.string().required(),

  // A localizacao deve ser uma string valida
  localizacao: Joi.string().required(),

});

module.exports = EmpreendimentosSchema;

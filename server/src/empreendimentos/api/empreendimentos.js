const createRoutes = require('../routes');
const router = require('express').Router();
const {
  EmpreendimentosController
} = require('../controllers');
const {
  EmpreendimentosService
} = require('../services');

module.exports = function empreendimentosAPI(app, Model) {
  const service = new EmpreendimentosService(Model);
  const routes = createRoutes(service, EmpreendimentosController);
  router.use('/empreendimentos', routes);
  app.use(router);
};

const mongoose = require('mongoose');

const empreendimentoSchema = new mongoose.Schema({
  nome: String,
  localizacao: String,
});

const EmpreendimentoModel = mongoose.model('empreendimento', empreendimentoSchema);

module.exports = EmpreendimentoModel;

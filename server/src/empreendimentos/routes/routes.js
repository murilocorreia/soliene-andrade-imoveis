const router = require('express').Router();

const createRoutes = (service, Controller) => {
  const controller = new Controller(service);
  router.get('/', controller.buscaTodos);
  return router;
}

module.exports = createRoutes;

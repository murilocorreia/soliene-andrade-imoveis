const httpStatus = require('http-status');

class EmpreendimentosController {
  constructor(service) {
    this.service = service;
    this.buscaTodos = this.buscaTodos.bind(this);
  }

  async buscaTodos(req, res) {
    try {
      await this.service.buscaTodos();
      res.status(httpStatus.OK).json();
    } catch (erro) {
      res.status(erro.httpCode).json({
        'erro': erro.message,
      });
    }
  }
}

module.exports = EmpreendimentosController;

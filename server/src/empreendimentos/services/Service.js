const httpStatus = require('http-status');
const empreendimentosSchema = require('../requisition-schema');
const winston = require('../../logger');
const {
  AppError,
  handleError,
} = require('../../error');

class Service {
  constructor(modelo) {
    this.Modelo = modelo;
  }

  gravar(empreendimento) {
    return new Promise(async (resolve, reject) => {
      try {
        winston.info('[EmpreendimentosService]:[gravar]');

        const dadosValidados = await this._validaInformacoesEmpreendimento(empreendimento);

        const empreendimentoModelo = new this.Modelo({
          nome: dadosValidados.nome,
          localizacao: dadosValidados.localizacao,
        });

        const dadoSalvo = await this._salvaNoBanco(empreendimentoModelo);
        resolve(dadoSalvo);
      } catch (erro) {
        winston.error(erro);
        reject(handleError(erro));
      }
    });
  }

  async _validaInformacoesEmpreendimento(empreendimento) {
    return new Promise(async (resolve, reject) => {
      try {
        const dadosValidados = await empreendimentosSchema
          .validate(empreendimento, {
            abortEarly: false,
          });
        resolve(dadosValidados);
      } catch (erroValidacao) {
        winston.error(erroValidacao.details.map(e => e.message));
        reject(new AppError('validacao', httpStatus.METHOD_NOT_ALLOWED,
          'Erro ao validar os dados recebidos', true));
        return;
      }
    });
  }

  _salvaNoBanco(empreendimentoModelo) {
    return new Promise(async (resolve, reject) => {
      try {
        const dadoSalvo = await empreendimentoModelo.save();
        winston.info(`Dados gravados ${dadoSalvo}`);
        resolve(dadoSalvo);
      } catch (erroSalvar) {
        winston.error(`Erro ao gravar dados de empreendimento ${erroSalvar}`);
        reject(new AppError('save', httpStatus.INTERNAL_SERVER_ERROR,
          'Erro ao gravar dados de empreendimento', true));
      }
    });
  }

  buscaTodos() {
    return new Promise(async (resolve, reject) => {
      try {
        winston.info('[EmpreendimentosService]:[buscaTodos]');
        const dados = await this.Modelo.find({}).exec();
        winston.info(`Dados encontrados ${dados}`);
        resolve(dados);
      } catch (erroInesperado) {
        winston.error(`Erro ao buscar dados de empreendimento ${erroInesperado}`);
        reject(new AppError('erro', httpStatus.INTERNAL_SERVER_ERROR,
          'Erro ao buscar os dados', false));
      }
    });
  }

}

module.exports = Service;

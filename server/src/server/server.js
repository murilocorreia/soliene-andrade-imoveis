const app = require('express')();
const consign = require('consign');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const winston = require('../logger');
const empreendimentosAPI = require('../empreendimentos/api');

const startServer = function startServer(models) {
  return new Promise((resolve, reject) => {
    app.use(bodyParser.urlencoded({
      extended: false,
    }));
    app.use(bodyParser.json());

    if (process.env.NODE_ENV !== 'test') {
      app.use(morgan('combined', {
        stream: winston.stream,
      }));
    }

    empreendimentosAPI(app, models.Empreendimentos);

    const server = app.listen(3000, (err) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(server);
    });
  });
};

module.exports = startServer;

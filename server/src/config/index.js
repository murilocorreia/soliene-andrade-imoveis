const {
  mongoUri,
  mongoOptions,
} = require('./mongo_config');

module.exports.mongoUri = mongoUri;
module.exports.mongoOptions = mongoOptions;

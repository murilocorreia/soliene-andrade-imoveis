let mongoUri;
if (process.env.NODE_ENV === 'production') {
  mongoUri = 'mongodb://db:27017/app-db';
} else {
  mongoUri = 'mongodb://localhost:27017/app-db';
}

const mongoOptions = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  autoIndex: false,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 500,
  poolSize: 10,
  bufferMaxEntries: 0,
  connectTimeoutMS: 10000,
  socketTimeoutMS: 45000,
  family: 4,
};

module.exports = Object.assign({}, {
  mongoUri,
  mongoOptions,
});

const winston = require('winston');

const logPath = './logs/';
const options = {
  file: {
    level: 'info',
    filename: `${logPath}app.log`,
    handleExceptions: true,
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.json(),
    ),
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false,
    timestamp: true,
  },
  error: {
    level: 'error',
    filename: `${logPath}error.log`,
    handleExceptions: true,
    format: winston.format.json(),
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false,
    timestamp: true,
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    format: winston.format.simple(),
    colorize: true,
  },
};

const logger = winston.createLogger({
  transports: [
    new winston.transports.File(options.file),
    new winston.transports.File(options.error),
    new winston.transports.Console(options.console),
  ],
  exitOnError: false,
});

module.exports = logger;
module.exports.stream = {
  write: function writeStream(message) {
    logger.info(message);
  },
};

const mongoose = require('mongoose');
const {
  mongoUri,
  mongoOptions,
} = require('./config');
const winston = require('./logger');
const startServer = require('./server/server');
const EmpreedimentosModel = require('../models');
const models = require('./empreendimentos/models');

mongoose.connect(mongoUri, mongoOptions).then(
  () => {
    winston.info('Sucesso ao conectar ao banco');
    winston.info('Iniciando servidor');
  },
).catch((erro) => {
  winston.error(`Erro ao conectar ao banco ${erro}`);
});

startServer(models).then(() => {
  winston.info('Server up');
}).catch((err) => {
  winston.error(`Error in server startup ${err}`);
});

module.exports = startServer;
